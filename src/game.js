// TODO: shoot delay (maybe many shoots)

var tanks = [];

function Tank(type) {
    this.type = type;
    this.x = Math.floor(Math.random() * (canvas.width - 2 * wall)) + wall;
    this.y = Math.floor(Math.random() * (canvas.height - 2 * wall)) + wall;
//            dx: 0,
//            dy: 0,
    this.max_spd = (type === 'bot') ? 1 : 2;
    this.spd = 0;
    this.dir = 0;
    this.tower_dir = 0;
    this.state = 0; // 2 - collision; 1 - new way after collision; 0 - normal move.

    if (type === 'bot') {
//            rnd: 0,
        this.interval = 0;
        this.way_x = 0; // destination way point
        this.way_y = 0; //
        this.way_dir = 0;
        this.way_angle = 0;
    }
}

Tank.prototype.r_collision = 41;

Tank.prototype.draw = function () {
    this.draw_body();
    this.draw_tower();
};

Tank.prototype.draw_body = function () {
    ctx.font = "10px Arial";

    ctx.fillStyle = 'rgb(36, 110, 40)';
    ctx.strokeStyle = 'rgb(25, 50, 20)';
    ctx.lineWidth = 2;

    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.dir * Math.PI / 180);

    ctx.drawImage(img.tank_body, -22.5, -38, 45, 76);
    ctx.restore();
};

Tank.prototype.draw_tower = function () {
    var x = this.x + 3 * Math.sin(this.dir * Math.PI / 180);
    var y = this.y - 3 * Math.cos(this.dir * Math.PI / 180);

    ctx.lineWidth = 3;
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(this.tower_dir * Math.PI / 180);
    ctx.drawImage(img.tank_tower, -17, -62, 34, 82.5);
    ctx.restore();
};


var canvas = document.getElementById("cnv");
var ctx = canvas.getContext("2d");

var img = {
    tank_body: 'img/body.png',
    tank_tower: 'img/tower.png',
    EXP: 'img/explosion.png',
    Wall: 'img/wall.png',
    Floor: 'img/floor.png'
};

var el, img_src;
for (el in img) {
    img_src = img[el];
    img[el] = new Image();
    img[el].src = img_src;
}


var tile_size = 32;

var NUM_BOTS = 10;
var TOLERANCE = 3;    // tolerance point ai moving

var map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

function init_map() {
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {
//            img,sx,sy,swidth,sheight,x,y,width,height
            if (map[i][j] === 0) {
                map[i][j] = rnd(16) + 10;
            }

            if (map[i][j] === 1) {
                map[i][j] = rnd(16) + 30;
            }
        }
    }
}
init_map();

function draw_map() {
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {
//            img,sx,sy,swidth,sheight,x,y,width,height
            if (map[i][j] < 26 && map[i][j] >= 10) {
                ctx.drawImage(img.Floor, tile_size * (map[i][j] - 10), 0, tile_size, tile_size, j * tile_size, i * tile_size, tile_size, tile_size);
            }
            if (map[i][j] < 46 && map[i][j] >= 30) {
                ctx.drawImage(img.Wall, tile_size * (map[i][j] - 30), 0, tile_size, tile_size, j * tile_size, i * tile_size, tile_size, tile_size);
            }
        }
    }
}

canvas.width = 1280;
canvas.height = 800;
var wall = 32;
var dump = 32;

var tank = new Tank('player');
tanks.push(tank);

//var tankX = 500;
//var tankY = 700;
//var tankSPD = 0;
//var tankDIR = 0;  // in degrees
//var towerDIR = 0;  // in degrees
//var towerX;
//var towerY;
var ammoSPD = 0;
var ammoSTAT = 0; // 0 - no shoot, 1 - fly, 2 - explosion
var shootDIR = 0;
var shootX;
var shootY;

var score = 0;

var keyUP = false;
var keyDOWN = false;
var keyLEFT = false;
var keyRIGHT = false;
var keySPACE = false;
var keySHIFT = false;
var keyA = false;
var keyD = false;


var arrow = {left: 37, up: 38, right: 39, down: 40};
var key = {space: 32, shift: 16, a: 65, d: 68};

function rnd(n) {
    return Math.floor(Math.random() * n);
}


function tank(x, y, r) {
    ctx.font = "10px Arial";

    ctx.fillStyle = 'rgb(36, 110, 40)';
    ctx.strokeStyle = 'rgb(25, 50, 20)';
    ctx.lineWidth = 2;

    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(r * Math.PI / 180);

    // tracks
    ctx.strokeRect(-22, -27, 8, 54);
    ctx.strokeRect(15, -27, 8, 54);

    // body
    ctx.fillRect(-20, -25, 41, 50);
    ctx.strokeRect(-20, -25, 41, 50);

    ctx.restore();
}

function tower(x, y, r) {
    ctx.lineWidth = 3;
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(r * Math.PI / 180);
    // gun
    ctx.fillRect(-2, -55, 5, 39);
    ctx.strokeRect(-2, -55, 5, 39);

    // tower
//    ctx.translate(0, -6);
//    ctx.lineWidth = 4;
    ctx.beginPath();
    ctx.arc(0, 0, 16, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(5, 2, 4, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.restore();
}


function ammo_calc() {
    if (ammoSTAT === 1) {
        var dy = ammoSPD * Math.cos(shootDIR * Math.PI / 180);
        var dx = ammoSPD * Math.sin(shootDIR * Math.PI / 180);

        shootX += dx;
        shootY -= dy;

        if (shootY < wall) {
            shootY = 0;
            ammoSTAT = 2;
        }
        if (shootY > canvas.height - wall) {
            shootY = canvas.height;
            ammoSTAT = 2;
        }
        if (shootX < wall) {
            shootX = 0;
            ammoSTAT = 2;
        }
        if (shootX >= canvas.width - wall) {
            shootX = canvas.width;
            ammoSTAT = 2;
        }
        if (ammoSTAT === 2) {
            delayEXP = 15;
        }
    } else if (ammoSTAT === 2) {
        delayEXP--;
        if (delayEXP <= 0) {
            ammoSTAT = 0;
        }
    }
}

function shoot(x, y, r) {
    ammoSTAT = 1;
    shootX = x;
    shootY = y;
    shootDIR = r;
    ammoSPD = 10;
}

function ammo_draw() {
    ctx.fillText('shootX: ' + Math.round(shootX), 50, 210);
    ctx.fillText('shootY: ' + Math.round(shootY), 50, 220);

    if (ammoSTAT === 1) {
        ctx.lineWidth = 1;
        ctx.save();
        ctx.translate(shootX, shootY);
        ctx.rotate(shootDIR * Math.PI / 180);
        // ammo
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(1, -3);
        ctx.lineTo(2, 0);
//        ctx.strokeRect(-0.5, 0, 3, 4);
        ctx.lineTo(2, 3);
        ctx.lineTo(0, 3);
        ctx.closePath();
        ctx.stroke();
        ctx.restore();
    } else if (ammoSTAT === 2) {
        ctx.save();
        ctx.translate(shootX, shootY);
        ctx.rotate(shootDIR * Math.PI / 180);
        ctx.drawImage(img.EXP, -15, 15);
        ctx.restore();

    }

}


function draw_test() {
//    ctx.drawImage(imgEXP, shootX, shootY);
}




function draw_score(tank) {
    ctx.fillStyle = 'rgb(0,0,0)';
    ctx.fillText(score, 50, 50);

    ctx.fillText('left: ' + keyLEFT, 50, 70);
    ctx.fillText('right: ' + keyRIGHT, 50, 80);
    ctx.fillText('up: ' + keyUP, 50, 90);
    ctx.fillText('down: ' + keyDOWN, 50, 100);
    ctx.fillText('space: ' + keySPACE, 50, 110);
    ctx.fillText('SHIFT: ' + keySHIFT, 50, 120);
    ctx.fillText('A: ' + keyA, 50, 130);
    ctx.fillText('D: ' + keyD, 50, 140);



    ctx.fillText('SPEED: ' + Math.round(tank.spd), 50, 150);
    ctx.fillText('Direction: ' + tank.dir, 50, 160);

    ctx.fillText('x: ' + Math.round(tank.x), 50, 170);
    ctx.fillText('y: ' + Math.round(tank.y), 50, 180);

}


function player_tank_move(tank) {
    var dx, dy;

    if (keySHIFT) {
        tank.max_spd = 3;
    } else {
        tank.max_spd = 2;
    }
    if (keyRIGHT) {
        if (tank.dir > 360) {
            tank.dir -= 360;
        }
        if (tank.spd < -0.1) {
            tank.dir--;
        } else {
            tank.dir++;
        }
    }
    if (keyLEFT) {
        if (tank.dir < 0) {
            tank.dir += 360;
        }
        if (tank.spd < -0.1) {
            tank.dir++;
        } else {
            tank.dir--;
        }
    }
    if (keyUP) {
        if (tank.spd < tank.max_spd) {
            tank.spd += 0.1;
        }
    }
    if (keyDOWN) {
        if (tank.spd > -1.5) {
            tank.spd -= 0.1;
        }
    }

    dy = tank.spd * Math.cos(tank.dir * Math.PI / 180);
    dx = tank.spd * Math.sin(tank.dir * Math.PI / 180);

    ctx.fillStyle = 'rgb(0,0,0)';
    ctx.font = "10px Arial";
    ctx.fillText('dx: ' + Math.round(dx * 100) / 100, 50, 190);
    ctx.fillText('dy: ' + Math.round(dy * 100) / 100, 50, 200);

    tank.x += dx;
    tank.y -= dy;

    if (tank.spd > 0) {
        tank.spd -= 0.05;
    }
    if (tank.spd < 0) {
        tank.spd += 0.05;
    }
    if (tank.spd > -0.05 && tank.spd < 0.05) {
        tank.spd = 0;
    }
}


function add_bots() {
    for (var ai = 0; ai < NUM_BOTS; ai++) {
        tanks.push(new Tank('bot'));
    }
}
add_bots();

function ai_tank_move(bot) {
    var dx, dy;
    var angle, angle_way, angle_tank;

    if (Math.abs(bot.way_x - bot.x) < TOLERANCE && Math.abs(bot.way_y - bot.y) < TOLERANCE || bot.way_x === 0 && bot.way_y === 0 || bot.state === 1) {
        bot.way_x = Math.round(Math.random() * (canvas.width - 3 * wall) + 1.5 * wall);
        bot.way_y = Math.round(Math.random() * (canvas.height - 3 * wall) + 1.5 * wall);
        bot.state = 0;
    }

    bot.way_dir = (Math.atan2(bot.way_y - bot.y, bot.way_x - bot.x) * 180 / Math.PI) + 90;

    if (bot.way_dir > 180) {
        angle_way = bot.way_dir - 360;
    } else {
        angle_way = bot.way_dir;
    }

    if (bot.dir > 180) {
        angle_tank = bot.dir - 360;
    } else {
        angle_tank = bot.dir;
    }

    angle = angle_way - angle_tank;
    bot.way_angle = (Math.abs(angle) > 180) ? angle - 360 * Math.sign(angle) : angle;

//// draw vector way
//    ctx.beginPath();
//    ctx.moveTo(bot.x, bot.y);
//    ctx.lineTo(bot.way_x, bot.way_y);
//    ctx.stroke();
////


//  if (bot.rnd > 6) {        // SHIFT
//    bot.maxSPD = 3;
//  } else {
//    bot.maxSPD = 2;
//  }
    if (Math.abs(bot.way_angle) < 2) {
        bot.dir = bot.way_dir;
    } else {
        if (bot.way_angle > 2 && bot.state === 0) {        // right
            if (bot.dir > 360) {
                bot.dir -= 360;
            }
            if (bot.spd < -0.1) {
                bot.dir--;
            } else {
                bot.dir++;
            }
        }
        if (bot.way_angle < 2 && bot.state === 0) {        // left
            if (bot.dir < 0) {
                bot.dir += 360;
            }
            if (bot.spd < -0.1) {
                bot.dir++;
            } else {
                bot.dir--;
            }
        }
    }
    if (Math.abs(bot.way_angle) < 3 && bot.state === 0) {        // forward
        if (bot.spd < bot.max_spd) {
            bot.spd += 0.1;
        }
    }
//    if (bot.rnd < 5) {        // backward
//        if (bot.spd > -1.5) {
//            bot.spd -= 0.2;
//        }
//    }

    dy = bot.spd * Math.cos(bot.dir * Math.PI / 180);
    dx = bot.spd * Math.sin(bot.dir * Math.PI / 180);

    bot.x += dx;
    bot.y -= dy;

    if (bot.spd > 0) {
        bot.spd -= 0.05;
    }
    if (bot.spd < 0) {
        bot.spd += 0.05;
    }
    if (bot.spd > -0.05 && bot.spd < 0.05) {
        bot.spd = 0;
    }

    bot.draw();
}


function tower_move(tank) {
    if (keyA) {
        tank.tower_dir = tank.tower_dir - 2;
    }
    if (keyD) {
        tank.tower_dir = tank.tower_dir + 2;
    }
    if (keyA && keyLEFT) {
        tank.tower_dir = tank.tower_dir - 0.5;
    }
    if (keyD && keyRIGHT) {
        tank.tower_dir = tank.tower_dir + 0.5;
    }
    if (keyA && keyRIGHT) {
        tank.tower_dir = tank.tower_dir + 0.5;
    }
    if (keyD && keyLEFT) {
        tank.tower_dir = tank.tower_dir - 0.5;
    }
}


var fps = 0;
var frame = 0;
var interval = Date.now();

function framesPerSecond() {
    frame++;

    if (Date.now() - interval > 1000) {
        fps = frame;
        interval = Date.now();
        frame = 0;
    }

    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.font = "20px Arial";
    ctx.fillText('FPS: ' + fps, 1180, 20);
}


function draw_frame() {
    ctx.fillStyle = 'gray';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    draw_map();
    for (var i in tanks) {
        if (tanks[i].type === 'bot') {

            ai_tank_move(tanks[i]);
        }
    }

    player_tank_move(tank);
    tower_move(tank);

//  if (keySPACE && ammoSTAT === 0) {
//    var x0 = towerX + 60 * Math.sin(towerDIR * Math.PI / 180);
//    var y0 = towerY - 60 * Math.cos(towerDIR * Math.PI / 180);
//    shoot(x0, y0, towerDIR);
//  }
    ammo_calc();
    ammo_draw();



    for (var t = 0; t < tanks.length; t++) {
        tanks[t].draw();
    }

    draw_score(tank);
    collision();

    score++;
    draw_test();
    framesPerSecond();

    requestAnimationFrame(draw_frame);
}


requestAnimationFrame(draw_frame);


document.onkeydown = function (e) {
    var keyCode = e.keyCode || e.which;

    switch (keyCode) {
        case arrow.left:
            keyLEFT = true;
            break;
        case arrow.up:
            keyUP = true;
            break;
        case arrow.right:
            keyRIGHT = true;
            break;
        case arrow.down:
            keyDOWN = true;
            break;
        case key.space:
            keySPACE = true;
            break;
        case key.shift:
            keySHIFT = true;
            break;
        case key.a:
            keyA = true;
            break;
        case key.d:
            keyD = true;
            break;

    }
};

document.onkeyup = function (e) {
    var keyCode = e.keyCode || e.which;

    switch (keyCode) {
        case arrow.left:
            keyLEFT = false;
            break;
        case arrow.up:
            keyUP = false;
            break;
        case arrow.right:
            keyRIGHT = false;
            break;
        case arrow.down:
            keyDOWN = false;
            break;
        case key.space:
            keySPACE = false;
            break;
        case key.shift:
            keySHIFT = false;
            break;
        case key.a:
            keyA = false;
            break;
        case key.d:
            keyD = false;
            break;
    }
};


/*
 * 1. 
 */
function collision() {


    for (var i = 0; i < tanks.length; i++) {
        for (var j = i; j < tanks.length; j++) {
            var old_way_x_i,
                old_way_x_j;
                    
            if (i !== j) {
                var distance = Math.sqrt(Math.pow(tanks[i].x - tanks[j].x, 2) + Math.pow(tanks[i].y - tanks[j].y, 2)),
                    collision_radius = tanks[i].r_collision + tanks[j].r_collision,
                    collision_dist = distance - collision_radius - 10;
                    console.log(collision_dist);
                    
                if (distance < (collision_radius - 10)) {
                    tanks[i].state = 2;
                    tanks[j].state = 2;
                    tanks[i].spd = 0;
                    tanks[j].spd = 0;
//                    debugger;
                    
                    var cross_dir_j = (Math.atan2(tanks[i].y - tanks[j].y, tanks[i].x - tanks[j].x,) * 180 / Math.PI) + 90;
                    var cross_dir_i = (Math.atan2(tanks[j].y - tanks[i].y, tanks[j].x - tanks[i].x,) * 180 / Math.PI) + 90;
                    var j_dir,
                        i_dir;
                   
                    j_dir = (tanks[j].dir > 180) ? tanks[j].dir - 360 : tanks[j].dir;
                    i_dir = (tanks[i].dir > 180) ? tanks[i].dir - 360 : tanks[i].dir;
                    
                    cross_dir_j = j_dir - cross_dir_j;
                    cross_dir_i = i_dir - cross_dir_i;
                    
                    cross_dir_j = (Math.abs(cross_dir_j) > 180) ? cross_dir_j - (360 * Math.sign(cross_dir_j)) : cross_dir_j;
                    cross_dir_i = (Math.abs(cross_dir_i) > 180) ? cross_dir_i - (360 * Math.sign(cross_dir_i)) : cross_dir_i;
                        
                    if (Math.abs(cross_dir_j) > 90){
                        tanks[j].spd = 1;
                    } else {
                        tanks[j].spd = -1;
                    }
                    
                    
                    if (Math.abs(cross_dir_i) > 90){
                        tanks[i].spd = 1;
                    } else {
                        tanks[i].spd = -1;
                    }
                    
                    old_way_x_i = tanks[i].way_x;
                    old_way_x_j = tanks[j].way_x;
                }
                if (Math.abs(collision_dist) >= 0 && Math.abs(collision_dist) < 100 && tanks[i].state === 2) {
                        tanks[i].state = 1;
                }
                if (Math.abs(collision_dist) >= 0 && Math.abs(collision_dist) < 100 && tanks[j].state === 2) {
                        tanks[j].state = 1;
                }
            }
        }

        if (tanks[i].y < wall) {
            tanks[i].y = dump;
            tanks[i].spd = 0;
        }
        if (tanks[i].y > canvas.height - wall) {
            tanks[i].y = canvas.height - dump;
            tanks[i].spd = 0;
        }
        if (tanks[i].x < wall) {
            tanks[i].x = dump;
            tanks[i].spd = 0;
        }
        if (tanks[i].x > canvas.width - wall) {
            tanks[i].x = canvas.width - dump;
            tanks[i].spd = 0;
        }
    }
}